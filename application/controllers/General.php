<?php
session_start();
class General extends CI_Controller
{
	public function _construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
	}

	public function signup()
	{
		$data["legit"] = "";
		$this->load->view('forme/signup', $data);
	}

	public function login()
	{
		$data["legit"] = "";
		$this->load->view('forme/login', $data);
	}

	public function ustvari_objavo()
	{
		$this->load->view('forme/ustvari_objavo');
	}

	public function ogled_izbris()
	{
		$this->load->model("products_model");
		$data["objava"] = $this->products_model->get_prod($_SESSION["upor"]);
		$this->load->view('ogled/ogled_izbris', $data);
	}

	public function izbris()
	{
		$this->load->model("products_model");
		$this->products_model->del_prod($_GET["nas"]);
		$data["naslov"] = $_GET["nas"];
		$this->load->view("izbrisano", $data);
	}

	public function izbris_admin()
	{
		$this->load->model("products_model");
		$naslov = $this->products_model->get_naslov($_GET["nasl"]);
		$this->products_model->del_prod($naslov);
		$data ["objava"] = $this->products_model->get_product();
		$data["admin"] = $_SESSION["admin"];
		$this->load->view('ogled/view_products', $data);
	}

	public function ogled_izdelka()
	{
		$this->load->model("products_model");
		$data ["obj"] = $this->products_model->get_specProd($_GET["nas"]);
		$data["reklama"] = $this->products_model->get_reklama($this->products_model->get_naslov($_GET["nas"]));
		$this->load->view("ogled/ogled_izdelka", $data);
	}

	public function view_products()
	{
		$this->load->model("products_model");
		$data ["objava"] = $this->products_model->get_product();
		$this->load->view('ogled/view_products', $data);
	}

	public function ogled_prod()
	{
		$this->load->model("products_model");
		$data ["objava"] = $this->products_model->get_prod($_SESSION["upor"]);
		$this->load->view('ogled/view_products', $data);
	}

	public function prijava_form()
	{
		$this->load->model("helper_model");
		if ($this->helper_model->num_user($_GET["upor"]) == 0 && $this->helper_model->num_ges($_GET["ges"]) == 0)
		{
			$_SESSION["upor"] = $_GET["upor"];
			$_SESSION["ges"] = $_GET["ges"];
			$_SESSION["admin"] = false;
			$this->helper_model->dodaj_prod($_GET["kon"],$_GET["upor"],$_GET["nas"],$_GET["ges"]);
			$this->load->view('prijavljen');
		} else {
			$data["legit"] = "Vneseni podatki niso vstrezi";
			$this->load->view('forme/signup', $data);
		}
	}

	public function login_form()
	{
		$this->load->model("helper_model");
		if ($_GET["upor"] == "admin" && $_GET["ges"] == "admin123")
		{
			$_SESSION["admin"] = true;
			$this->load->view('admin');
		}
		else if ($this->helper_model->num_row($_GET["upor"], $_GET["ges"]) == 1)
		{
			$_SESSION["admin"] = false;
			$_SESSION["upor"] = $_GET["upor"];
			$_SESSION["ges"] = $_GET["ges"];
			$this->load->view('prodajalec');
		} else {
			$data["legit"] = "Neveljavni podatki";
			$this->load->view('forme/login', $data);
		}
	}

	public function prodajalec()
	{
		$this->load->view('prodajalec');
	}

	public function objavi()
	{
		$this->load->model("helper_model");
		$this->helper_model->dodaj_objavo($_SESSION["upor"],$_GET["opis"],$_GET["slika"],$_GET["naslov"]);
		$this->ogled_prod();
	}

	public function status()
	{
		$this->load->model("products_model");
		$this->products_model->status($_GET["nasl1"]);
		$data ["objava"] = $this->products_model->get_product();
		$this->load->view('ogled/view_products', $data);
	}

	public function uporabniki()
	{
		$this->load->model("helper_model");
		$data["prodajalec"] = $this->helper_model->get_usesrs();
		$this->load->view('ogled/view_users', $data);
	}

	public function del_user()
	{
		$this->load->model("helper_model");
		$this->helper_model->del_user($_GET["delU"]);
		$this->load->view('admin');
	}

}

?>