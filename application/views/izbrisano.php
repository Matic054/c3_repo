<?php
$this->load->helper("url_helper");
?>
<!DOCTYPE html>
<html>
<head>
	<title>izbrisano</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Trirong">
	<style type="text/css">
		text {
			font-family: "Trirong", serif;
		}
	    a{
	    	font-family: Arial, Helvetica, sans-serif;
	    	border-style: solid;
	    	border-radius: 5px;
	    	text-decoration:none;
		    color: blue;
		    font-size: 200%;
		    padding: 1%;
		    background-color: white;
		}

		h1 {
			font-family: Arial, Helvetica, sans-serif;
  			font-size: 50px;
			border-width: 3px;
			border-style: solid;
			border-radius: 20px;
			border-color: blue;
			color: blue;
			font-style: bold;
			text-align:center;
			background-color: white;
			padding: 4%;
		}

		body {
			background-color: cornflowerblue;
		}
	</style>
</head> 
<body>
<h1> IZBRIS VSPESEN </h1>
<br> <br> <br>
<a style="margin-left: 37%">Objava "<?php echo $naslov?>" je izbrisana</a>
<br><br><br><br>
<?php $this->load->helper("url_helper");?>
<a style="margin-left: 46.5%" href="<?php echo site_url('/'); ?>">Odjava</a>
</body>
</html>
