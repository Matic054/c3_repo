<!DOCTYPE html>
<html>
<head>
	<title>Izdelki</title>
	<style type="text/css">
		body{
			background-color: cornflowerblue;
		}
		input{
			font-size: 125%;
			float:left;
			border-style: solid;
			border-width: 2px;
			border-radius: 5px;
			border-color: black;
		}
		h3{
			font-family: Arial, Helvetica, sans-serif;
		}
		p {
			font-family: Arial, Helvetica, sans-serif;
		}
		a{
	    	font-family: Arial, Helvetica, sans-serif;
	    	border-style: solid;
	    	border-radius: 5px;
	    	text-decoration:none;
		    color: blue;
		    font-size: 125%;
		    padding: 0.5%;
		    background-color: white;
		    margin-left: 46%;
		}
	</style>
</head>
<body>
<?php 
$this->load->helper("url_helper");
$this->load->model("products_model");
$int = -1; // index uporabljen pri ogledu specificne objave
?>
<?php foreach ($objava as $ob): $int = $int + 1;?>
		<?php
			if ($this->products_model->get_poreklo($ob['naslov']) == FALSE)
			{
		?>
			<div style="border-style: solid; height: 120px; background-color:red; margin:10px; margin-left:100px; margin-right:100px">
		<?php
			} else {
		?>
			<div style="border-style: solid; height: 120px; background-color:lightgreen; margin:10px; margin-left:100px; margin-right:100px">
		<?php
			}
		?>
		<?php echo " <img src='".$ob['slika']."' alt='slika' style='float:left;width:140px;height:120px;border-right-style: solid;'>"; ?>
		<h3 style="float:left;"><?php echo $ob['naslov']; ?></h3>
		<br>
		<br>
        <p style="float:left"><?php echo $ob['opis']; ?></p>
        <br>
        <br>
        <?php
        	if ($_SESSION["admin"])
        	{
        		?> 
        			<form action="status" method="get">
        			<input style="visibility: hidden; float:left;width:0px;" type="number" id="nasl1" name="nasl1" value=<?php echo $int; ?>>
        			<input type="submit" value="status" style="margin-right: 10px">
        			</form>
        			<form action="izbris_admin" method="get">
        			<input type="submit" value="izbrisi">
        			<input style="visibility: hidden; float:left;width:0px;" type="number" id="nasl" name="nasl" value=<?php echo $int; ?>>
        		    </form>
        		<?php
        	}
        ?>
        <form action="ogled_izdelka" method="get">
        <input type="submit" value="ogled">
        <input style="visibility: hidden; float:left;" type="number" id="nas" name="nas" value=<?php echo $int; ?>>
        <br>
        <br>
		</form>
        </div>

  
<?php endforeach;?>
<br>
<?php
        	if ($_SESSION["admin"])
        	{
        		?> 
<a href="<?php echo site_url('/'); ?>">Odjava</a>
<?php 
			} else {
			?>
<a href="<?php echo site_url('/'); ?>">Domov</a>
			<?php
			}
?>
</body>
</html>