<!DOCTYPE html>
<html>
<head>
	<title>Uporabniki</title>
	<style type="text/css">
		body{
			background-color: cornflowerblue;
		}
		input{
			font-size: 125%;
			float:left;
			border-style: solid;
			border-width: 2px;
			border-radius: 5px;
			border-color: black;
		}
		h3{
			font-family: Arial, Helvetica, sans-serif;
		}
		p {
			font-family: Arial, Helvetica, sans-serif;
		}
		a{
	    	font-family: Arial, Helvetica, sans-serif;
	    	border-style: solid;
	    	border-radius: 5px;
	    	text-decoration:none;
		    color: blue;
		    font-size: 125%;
		    padding: 0.5%;
		    background-color: white;
		}
		div {
			padding-left: 20px;
			padding-bottom: 40px;
			color: blue;
			background-color: white;
			border-style: solid;
			border-width: 2.5px;
			border-color: blue;
			border-radius: 7px;
		}
		form {
			float:left;
		}
	</style>
</head>
<body>
<?php 

$this->load->helper("url_helper");

?>
<?php foreach ($prodajalec as $prod): ?>
<div>
<h3>UPORABNIK: <?php echo $prod["uime"]?> </h3>
<p>KONTAKT: <?php echo $prod["kontakt"]?> </p>
<form action="del_user" method="get">
    <input style="visibility: hidden; float:left;width:0px;" type="text" id="delU" name="delU" value=<?php echo $prod["uime"]; ?>>
    <input type="submit" value="izbrisi uporanika" style="margin-right: 10px">
</form>
</div>
<br>
<?php endforeach;?>
</body>
</html>