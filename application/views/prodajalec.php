<?php
$this->load->helper("url_helper");
?>

<!DOCTYPE html>
<html>
<head>
	<title>prodajalec</title>
	<style type="text/css">
	    a{
	    	font-family: Arial, Helvetica, sans-serif;
	    	border-style: solid;
	    	border-radius: 5px;
	    	text-decoration:none;
		    color: blue;
		    font-size: 150%;
		    padding: 0.5%;
		    background-color: white;
		}

		a:hover {
			background-color: lightgray;
		}

		h1 {
			font-family: Arial, Helvetica, fantasy;
			font-size: 50px;
			border-width: 3px;
			border-style: solid;
			border-radius: 20px;
			border-color: blue;
			color: blue;
			font-style: bold;
			text-align:center;
			background-color: white;
			padding: 4%;
		}

		body {
			background-color: cornflowerblue;
		}
	</style>
</head>
<body>
<h1 style="text-align: center">Vpis je bil vspesen.</h1>
<br><br><br><br>
<a style="margin-left: 45%" href="<?php echo site_url('general/ogled_prod'); ?>">Vase objave</a>
<br><br><br>
<a style="margin-left: 45%" href="<?php echo site_url('general/ustvari_objavo'); ?>">Nova objava</a>
<br><br><br>
<a style="margin-left: 45%" href="<?php echo site_url('general/ogled_izbris'); ?>">Izbrisi objavo</a>
<br><br><br>
<?php $this->load->helper("url_helper");?>
<a style="margin-left: 46.5%" href="<?php echo site_url('/'); ?>">Odjava</a>
</body>
</html>