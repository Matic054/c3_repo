<?php

class Products_model extends CI_Model
{
	    public function __construct()
        {
                $this->load->database();
        }

         public function get_product()
        {
            $query = $this->db->query(" SELECT * FROM objava");
            return $query->result_array();
        }

        public function get_prod($par)
        {
            $query = $this->db->query(" SELECT * FROM `objava` WHERE idp IN (SELECT uime FROM `prodajalec` WHERE uime ='".$par."')");
            return $query->result_array();
        }

        public function get_poreklo($nas)
        {
            return (count($this->db->query(" SELECT * FROM `objava` WHERE naslov ='".$nas."' AND potP = 1")->result_array()) == 1);
        }

        public function poreklo($nas)
        {
            return $this->db->query(" SELECT * FROM `objava` WHERE naslov ='".$nas."'")->result_array()[0]["potP"];
        }

        public function status($nas)
        {
            $naslov = $this->get_specNaslov($nas);
            if (!$this->get_poreklo($naslov))
            {
                return $this->db->query(" UPDATE `objava` SET `potP` = '1' WHERE `objava`.`naslov` = '".$naslov."';");
            } else {
                return $this->db->query(" UPDATE objava SET potP = '0' WHERE naslov = '".$naslov."';");
            }
        }

        public function del_prod($nas)
        {
        	return $this->db->query("DELETE FROM objava WHERE naslov = '".$nas."';");
        }

        public function get_specProd($ind)
        {
            return $this->get_product()[$ind];
        }

        public function get_specNaslov($ind)
        {
            return $this->get_product()[$ind]["naslov"];
        }

        public function get_naslov($ind)
        {
            return $this->get_product()[$ind]["naslov"];
        }

        public function get_reklama($nas)
        {
            $query = $this->db->query(" SELECT * FROM reklama WHERE id IN (SELECT idr FROM objava WHERE naslov ='".$nas."')");
            return $query->result_array();
        }
}

?>